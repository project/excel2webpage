<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Drupal\Core\Database\Database;
use Drupal\excel2webpage\CustomExcelTableHelper;
use Drupal\excel2webpage\Entity\CustomExcelTable;
use Drupal\file\Entity\File;

/**
 * Implements hook_cron().
 */
function excel2webpage_cron() {

  $excel_config = \Drupal::config('excel2webpage.settings');
  $cron_count = $excel_config->get('excel_cron_number');

  //Gettig excel file which are not imported in temp table
  $entity_query = Drupal::entityQuery('custom_excel_table')
    ->condition('cron_flag', 0)
    ->range(0, $cron_count)
    ->sort('id', 'DESC');

  $entity_list = $entity_query->execute();

  //Looping through entity list
  foreach ($entity_list as $entity_id) {
    //loading entity object
    $entity = CustomExcelTable::load($entity_id);

    if (!$entity->get('excel_file')->isEmpty()) {
      $excel_file_id = $entity->get('excel_file')->getValue()[0]['target_id'];
      $excel_file = File::load($excel_file_id);
      $excel_sheet = $entity->get('excel_sheet')->getValue()[0]['value'];
      $json_final_data = CustomExcelTableHelper::getExcelFileData($excel_file, $excel_sheet);

      if ($json_final_data) {
        $table_schema = [];
        $json_shift = (array) array_shift($json_final_data);
        $json_headers = CustomExcelTableHelper::getExcelHeader($excel_file, $excel_sheet);

        //Creating table schema
        $table_schema = [
          'fields' => [
            'et_id' => ['type' => 'serial', 'not null' => TRUE],
          ],
          'primary key' => ['et_id'],
        ];

        //Creating schema for db table
        foreach ($json_headers as $header_field_name => $header_title) {
          if (!empty($header_title)) {
            $table_schema['fields'][$header_field_name] = [
              'type' => 'text',
              'size' => 'big',
            ];
          }
        }

        //Creating DB
        $tablename = 'et_' . CustomExcelTableHelper::getFieldName($excel_file->getFilename()) . '_' . $entity_id;

        $excel_config =\Drupal::config('excel2webpage.settings');
        $db_connect = $excel_config->get('excel_db_connect');

        //Set active connection
        Database::setActiveConnection($db_connect);

        if (Database::getConnection()
          ->schema()->tableExists($tablename)) {
          Database::getConnection()
            ->schema()->dropTable($tablename);
        }

        $db_table = Database::getConnection()
          ->schema()
          ->createTable($tablename, $table_schema);


        //Saving data into json structure with entity
        $table_json_data = [
          'tablename' => $tablename,
          'fields' => $json_headers,
        ];

        if (!empty($json_final_data)) {
          //delete all records from table
          Database::getConnection()
            ->delete($tablename)
            ->execute();

          //Looping through json data
          foreach ($json_final_data as $json_item) {

            if (is_array($json_item)) {
              unset($json_item['']);
              Database::getConnection()
                ->insert($tablename)
                ->fields($json_item)
                ->execute();
            }
          }

        }

        //Set default active connection
        Database::setActiveConnection('default');

        //adding message to watchdog.
        \Drupal::logger('excel2webpage')
          ->notice('Excel file ' . $excel_file->getFilename() . " is imported");

        $entity->set('table_info', json_encode($table_json_data));
        $entity->set('cron_flag', 1);
        $entity->save();
      }
      else {
        \Drupal::logger('excel2webpage')
          ->notice('Excel file data not found ');
      }
    }
    else {
      \Drupal::logger('excel2webpage')->notice('Excel file not found ');
    }
  }
}
