<?php

namespace Drupal\excel2webpage\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\excel2webpage\CustomExcelTableHelper;
use Drupal\excel2webpage\Entity\CustomExcelTable;
use Drupal\file\Entity\File;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Form controller for the manage_inventory entity edit forms.
 *
 * @ingroup commerce_multivendor
 */
class CustomExcelTableSelectionForm extends FormBase {

  protected $sort_number_count = 1;

  protected $filter_number_count = 1;

  protected $display_number_count = 1;

  protected $display_column_count = 1;

  protected $remove_filter_item = [];

  protected $remove_sort_item = [];

  protected $display_option_array = [0 => 1];

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'custom_excel_table_selection_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Getting excel entity.
    $entity_id = \Drupal::request()->query->get('entity_id');
    if (!empty($entity_id)) {

      $excel_entity = CustomExcelTable::load($entity_id);

      // Getting Excel Sorting.
      $entity_excel_sort = [];
      $entity_excel_filter = [];
      $entity_excel_show = [];
      $default_display_values = [];

      // If there is excel sort.
      if (!$excel_entity->get('excel_sort')->isEmpty()) {
        $entity_excel_sort = $excel_entity->get('excel_sort')->getValue();
        $this->sort_number_count = count($entity_excel_sort);
      }

      // If there is excel filter.
      if (!$excel_entity->get('excel_filter')->isEmpty()) {
        $entity_excel_filter = $excel_entity->get('excel_filter')->getValue();
        $this->filter_number_count = count($entity_excel_filter);
      }

      // If there is show option for excel entity.
      if (!$excel_entity->get('excel_show')->isEmpty()) {
        $entity_excel_show = $excel_entity->get('excel_show')->getValue();
        $this->display_number_count = count($entity_excel_show);
      }

      if (!$excel_entity->get('default_display_header')->isEmpty()) {
        $default_display_values = array_values((array) json_decode($excel_entity->get('default_display_header')
          ->getValue()[0]['value']));
      }

      // If there is sort number count.
      if ($form_state->has('sort_number_count')) {
        $this->sort_number_count = $form_state->get('sort_number_count');
      }

      if ($form_state->has('filter_number_count')) {
        $this->filter_number_count = $form_state->get('filter_number_count');
      }

      if ($form_state->has('display_number_count')) {
        $this->display_number_count = $form_state->get('display_number_count');
      }

      $form['#tree'] = TRUE;

      $excel_sheet = (!$excel_entity->get('excel_sheet')
        ->isEmpty() ? $excel_entity->get('excel_sheet')
        ->getValue()[0]['value'] : 0);
      $header_option = [];

      $excel_file_fid = $excel_entity->get('excel_file')
        ->getValue()[0]['target_id'];
      $excel_file = File::load($excel_file_fid);
      $header_option = CustomExcelTableHelper::getExcelHeader($excel_file, $excel_sheet);
      $filter_option = ['' => 'Select Column'] + $header_option;

      $form['sorting'] = [
        '#type' => 'fieldset',
        '#title' => 'Excel Sorting',
      ];

      $form['sorting']['table'] = [
        '#type' => 'table',
        '#header' => [t('Select Sorting'), t('Ordering'), t('Action')],
      // CHECK THIS ID.
        '#attributes' => ['id' => 'excel-sorting-wrap'],
      ];

      // Getting total sort count.
      $total_sort_count = $this->sort_number_count;

      // If there is remove.
      if ($form_state->has('remove_sort_count')) {
        $total_sort_count = $form_state->get('remove_sort_count');
      }

      for ($sort_counter = 0; $sort_counter < $total_sort_count; $sort_counter++) {

        if (!in_array($sort_counter, $this->remove_sort_item)) {

          $sort_column_name = '';
          $sort_operator = '';
          $sort_entity_id = 0;

          if (!empty($entity_excel_sort[$sort_counter])) {
            $sort_excel_entity = Paragraph::load($entity_excel_sort[$sort_counter]['target_id']);
            $sort_column_name = (!$sort_excel_entity->get('field_excel_sort_name')
              ->isEmpty() ? $sort_excel_entity->get('field_excel_sort_name')
              ->getValue()[0]['value'] : '');
            $sort_operator = (!$sort_excel_entity->get('field_excel_sort_order')
              ->isEmpty() ? $sort_excel_entity->get('field_excel_sort_order')
              ->getValue()[0]['value'] : '');
            $sort_entity_id = $sort_excel_entity->id();
          }
          $form['sorting']['table'][$sort_counter]['column_name'] = [
            '#type' => 'select',
            '#title' => 'Column Name',
            '#options' => $filter_option,
            '#default_value' => $sort_column_name,
          ];

          $form['sorting']['table'][$sort_counter]['sorting_type'] = [
            '#type' => 'select',
            '#title' => 'Ordering',
            '#options' => [
              'ASC' => 'ASC',
              'DESC' => 'DESC',
            ],
            '#default_value' => $sort_operator,
          ];

          $form['sorting']['table'][$sort_counter]['remove_item'] = [
            '#type' => 'submit',
            '#value' => $this->t('Remove'),
            '#submit' => ['::customExcelRemoveSortItem'],
            '#name' => 'sorting_remove_item_' . $sort_counter,
            // Since we are removing a name, don't validate until later.
            '#limit_validation_errors' => [],
            '#ajax' => [
              'callback' => '::customExcelSortAddAjax',
          // CHECK THIS ID.
              'wrapper' => 'excel-sorting-wrap',
            ],
            '#sort_counter' => $sort_counter,
          ];

          $form['sorting']['table'][$sort_counter]['entity_id'] = [
            '#type' => 'hidden',
            '#value' => $sort_entity_id,
          ];
        }
      }

      // Disable caching on this form.
      $form_state->setCached(FALSE);

      $form['sorting']['actions'] = [
        '#type' => 'actions',
      ];

      $form['sorting']['actions']['add_item'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add Another Sorting'),
        '#submit' => ['::customExcelAddSortItem'],
        '#name' => 'sorting_add_item',
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::customExcelSortAddAjax',
          'wrapper' => 'excel-sorting-wrap',
        ],
      ];

      $form['filter'] = [
        '#type' => 'fieldset',
        '#title' => 'Excel Filter',
      ];

      $form['filter']['filter_message'] = [
        '#type' => 'item',
        '#markup' => '<p>If column label entered for filter, then it will display instead of column name</p>',
      ];

      $form['filter']['table'] = [
        '#type' => 'table',
        '#header' => [
          t('Filter Label'),
          t('Select Filter'),
          t('Field Type'),
          t('Operator'),
          t('Action'),
        ],
        '#attributes' => ['id' => 'excel-filter-wrap'],
      ];

      // Getting total sort count.
      $total_filter_count = $this->filter_number_count;

      for ($filter_counter = 0; $filter_counter < $total_filter_count; $filter_counter++) {

        if (!in_array($filter_counter, $this->remove_filter_item)) {

          $filter_column_name = '';
          $filter_field_type = '';
          $filter_field_operator = '';
          $filter_entity_id = 0;
          $filter_column_label = '';

          if (!empty($entity_excel_filter[$filter_counter])) {
            $filter_excel_entity = Paragraph::load($entity_excel_filter[$filter_counter]['target_id']);
            $filter_column_name = (!$filter_excel_entity->get('field_excel_filter_field_name')
              ->isEmpty() ? $filter_excel_entity->get('field_excel_filter_field_name')
              ->getValue()[0]['value'] : '');
            $filter_column_label = (!$filter_excel_entity->get('field_excel_filter_label')
              ->isEmpty() ? $filter_excel_entity->get('field_excel_filter_label')
              ->getValue()[0]['value'] : '');
            $filter_field_type = (!$filter_excel_entity->get('field_excel_filter_field_type')
              ->isEmpty() ? $filter_excel_entity->get('field_excel_filter_field_type')
              ->getValue()[0]['value'] : '');
            $filter_field_operator = (!$filter_excel_entity->get('field_excel_filter_operator')
              ->isEmpty() ? $filter_excel_entity->get('field_excel_filter_operator')
              ->getValue()[0]['value'] : '');
            $filter_entity_id = $filter_excel_entity->id();
          }

          $form['filter']['table'][$filter_counter]['column_label'] = [
            '#type' => 'textfield',
            '#title' => 'Column Label',
            '#default_value' => $filter_column_label,
            '#attributes' => ['placeholder' => 'Enter custom Label if required instead of column label.'],
            '#size' => 15,
          ];

          $form['filter']['table'][$filter_counter]['column_name'] = [
            '#type' => 'select',
            '#title' => 'Column Name',
            '#options' => $filter_option,
            '#default_value' => $filter_column_name,
          ];

          $form['filter']['table'][$filter_counter]['field_type'] = [
            '#type' => 'select',
            '#title' => 'Ordering',
            '#options' => [
              'textfield' => 'TextField',
              'select' => 'Select',
            ],
            '#default_value' => $filter_field_type,
          ];

          $form['filter']['table'][$filter_counter]['operator'] = [
            '#type' => 'select',
            '#title' => 'Ordering',
            '#options' => [
              'AND' => 'AND',
              'OR' => 'OR',
            ],
            '#default_value' => $filter_field_operator,
          ];

          $form['filter']['table'][$filter_counter]['remove_item'] = [
            '#type' => 'submit',
            '#value' => $this->t('Remove'),
            '#submit' => ['::customExcelRemoveFilterItem'],
            '#name' => 'filter_remove_item_' . $filter_counter,
            // Since we are removing a name, don't validate until later.
            '#limit_validation_errors' => [],
            '#ajax' => [
              'callback' => '::customExcelFilterAddAjax',
          // CHECK THIS ID.
              'wrapper' => 'excel-filter-wrap',
            ],
            '#filter_counter' => $filter_counter,
          ];

          $form['filter']['table'][$filter_counter]['entity_id'] = [
            '#type' => 'hidden',
            '#value' => $filter_entity_id,
          ];
        }
      }

      // Disable caching on this form.
      $form_state->setCached(FALSE);

      $form['filter']['actions'] = [
        '#type' => 'actions',
      ];

      $form['filter']['actions']['add_item'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add Another Filter'),
        '#submit' => ['::customExcelAddFilterItem'],
        '#name' => 'filter_add_item',
        '#ajax' => [
          'callback' => '::customExcelFilterAddAjax',
          'wrapper' => 'excel-filter-wrap',
        ],
        '#limit_validation_errors' => [],
      ];

      $form['default_display'] = [
        '#type' => 'checkboxes',
        '#title' => 'Select Column for Display Prepopulation',
        '#options' => $header_option,
        '#multiple' => TRUE,
        '#default_value' => $default_display_values,
      ];

      $form['display_show'] = [
        '#type' => 'fieldset',
        '#title' => 'Display Show',
      ];

      $form['display_show']['display'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'excel-display-wrap'],
      ];

      $total_display_count = 1;

      // If there is remove.
      if ($form_state->has('remove_display_count')) {
        $total_display_count = $form_state->get('remove_display_count');
      }

      $remove_display_item = '';
      if ($form_state->has('remove_display_item')) {
        $remove_display_item = $form_state->get('remove_display_item');
      }

      $current_display_counter = 0;
      if ($form_state->has('current_display')) {
        $current_display_counter = $form_state->get('current_display');
      }

      $formuser_input = $form_state->getUserInput();

      $display_value = (!empty($formuser_input['display_show']) ? $formuser_input['display_show']['display'] : []);
      $display_counter = 0;

      $show_column_name = '';
      $show_option_list = [];
      $display_entity_id = 0;
      $option_columns = [];

      if (!empty($entity_excel_show[$display_counter])) {
        $show_excel_entity = Paragraph::load($entity_excel_show[$display_counter]['target_id']);
        $show_column_name = (!$show_excel_entity->get('field_excel_show_field_label')
          ->isEmpty() ? $show_excel_entity->get('field_excel_show_field_label')
          ->getValue()[0]['value'] : '');
        $option_columns = (!$show_excel_entity->get('field_excel_display_column')
          ->isEmpty() ? $show_excel_entity->get('field_excel_display_column')
          ->getValue() : []);
        $display_entity_id = $show_excel_entity->id();

        // If there is option column for show.
        if (!empty($option_columns)) {
          $option_total_count = count($option_columns);
          $this->display_column_count = $option_total_count;
        }
      }

      if ($form_state->has('display_column_count')) {
        $this->display_column_count = $form_state->get('display_column_count');
      }

      $form['display_show']['display'][$display_counter]['display_label'] = [
        '#type' => 'textfield',
        '#title' => 'Show Label',
        '#default_value' => $show_column_name,
      ];

      $form['display_show']['display'][$display_counter]['option'] = [
        '#type' => 'fieldset',
        '#title' => 'Select Option for Display',
      ];

      $form['display_show']['display'][$display_counter]['option']['option_wrap'] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => 'excel-display-wrap-option-' . $display_counter,
        ],
      ];

      $form['display_show']['display'][$display_counter]['entity_id'] = [
        '#type' => 'hidden',
        '#value' => $display_entity_id,
      ];

      $option_total_count = $this->display_column_count;

      for ($option_counter = 0; $option_counter < $option_total_count; $option_counter++) {

        $option_label = '';
        $option_column_names = [];
        $default_option_column = [];
        $option_display_entity_id = 0;

        // If there is value for option list.
        if (!empty($option_columns[$option_counter])) {
          $option_display_entity = Paragraph::load($option_columns[$option_counter]['target_id']);
          $option_label = (!$option_display_entity->get('field_excel_column_label')
            ->isEmpty() ? $option_display_entity->get('field_excel_column_label')
            ->getValue()[0]['value'] : '');
          $option_column_names = (!$option_display_entity->get('field_excel_display_option')
            ->isEmpty() ? $option_display_entity->get('field_excel_display_option')
            ->getValue() : []);
          $option_display_entity_id = $option_display_entity->id();
        }

        foreach ($option_column_names as $col_value) {
          $default_option_column[] = $col_value['value'];
        }

        $form['display_show']['display'][$display_counter]['option']['option_wrap'][$option_counter] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['excel-display-wrap-option']],
        ];

        if ($option_counter == 0) {
          $message_text = '<p>Select option for Default display to render in front end.';
        }
        else {
          $message_text = '<p>Select option for Display to render in front end.';
        }

        $form['display_show']['display'][$display_counter]['option']['option_wrap'][$option_counter]['display_message'] = [
          '#type' => 'item',
          '#markup' => $message_text,
        ];

        $form['display_show']['display'][$display_counter]['option']['option_wrap'][$option_counter]['default_check'] = [
          '#type' => 'button',
          '#value' => 'Copy Column From Display Prepopulation',
          '#attributes' => ['class' => ['copy-default-display']],
        ];

        $form['display_show']['display'][$display_counter]['option']['option_wrap'][$option_counter]['option_label'] = [
          '#type' => 'textfield',
          '#title' => 'Option Label',
          '#default_value' => $option_label,
        ];

        $form['display_show']['display'][$display_counter]['option']['option_wrap'][$option_counter]['option_column'] = [
          '#type' => 'select',
          '#title' => 'Show/Hide Column',
          '#options' => $filter_option,
          '#multiple' => TRUE,
          '#default_value' => $default_option_column,
          '#attributes' => ['id' => 'display-option-column-names-' . $option_counter],
          '#prefix' => '<div class="display-option-column-wrap">',
          '#suffix' => '</div>',
        ];

        $form['display_show']['display'][$display_counter]['option']['option_wrap'][$option_counter]['entity_id'] = [
          '#type' => 'hidden',
          '#value' => $option_display_entity_id,
        ];
      }

      $form['display_show']['display'][$display_counter]['option']['actions'] = [
        '#type' => 'actions',
      ];

      $form['display_show']['display'][$display_counter]['option']['actions']['add_item'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add Another Option'),
        '#submit' => ['::customExcelAddDisplayOptionItem'],
        '#counter' => $display_counter,
        '#name' => 'display_option_add_item' . $display_counter,
        '#ajax' => [
          'callback' => '::customExcelDisplayOptionAddAjax',
          'wrapper' => 'excel-display-wrap-option-' . $display_counter,
        ],
        '#limit_validation_errors' => [],
      ];

      $form['display_show']['actions'] = [
        '#type' => 'actions',
      ];

      $form['excel_header'] = [
        '#type' => 'value',
        '#value' => $header_option,
      ];

      $form['entity_id'] = [
        '#type' => 'hidden',
        '#value' => $entity_id,
      ];

      $form['actions'] = [
        '#type' => 'actions',
      ];

      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#button_type' => 'primary',
        '#value' => $this->t('Submit'),
      ];

      $form['actions']['back'] = [
        '#type' => 'submit',
        '#value' => $this->t('Back'),
        '#submit' => ['::customExcelBackSubmit'],
        '#limit_validation_errors' => [],
      ];

      $form['#attached']['library'][] = 'excel2webpage/admin';

    }
    else {
      $form['message'] = [
        '#type' => 'item',
        '#markup' => 'There is no excel entity exists. Please create entity using ' . Link::fromTextAndUrl(t('Add Excel Table'), Url::fromRoute('entity.custom_excel_table.add_form'))
          ->toString(),
      ];
    }

    return $form;
  }

  /**
   * @param $form
   * @param $form_state
   *
   * @return mixed
   */
  public function customExcelSortAddAjax($form, $form_state) {
    return $form['sorting']['table'];
  }

  /**
   * Submit callback: Add New Sort Item.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function customExcelAddSortItem(array &$form, FormStateInterface $form_state) {
    $this->sort_number_count++;
    $form_state->set('sort_number_count', $this->sort_number_count);
    $form_state->setRebuild();
  }

  /**
   * Ajax callback: Add Display Item.
   *
   * @param $form
   * @param $form_state
   *
   * @return mixed
   */
  public function customExcelDisplayAddAjax($form, $form_state) {
    return $form['display_show']['display'];
  }

  /**
   * Submit callback: Add Display item.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function customExcelAddDisplayItem(array &$form, FormStateInterface $form_state) {
    $counter = $this->display_number_count;
    $this->display_number_count++;
    $this->display_option_array[$counter] = 1;
    $form_state->set('display_number_count', $this->display_number_count);
    $form_state->setRebuild();
  }

  /**
   * Ajax callback: Add Display Option.
   *
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public function customExcelDisplayOptionAddAjax($form, FormStateInterface $form_state) {
    $counter = $form_state->getTriggeringElement()['#counter'];
    return $form['display_show']['display'][$counter]['option']['option_wrap'];
  }

  /**
   * Submit callback: Add Display option.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function customExcelAddDisplayOptionItem(array &$form, FormStateInterface $form_state) {
    $counter = $form_state->getTriggeringElement()['#counter'];
    $this->display_column_count++;
    $form_state->set('display_column_count', $this->display_column_count);
    $form_state->set('current_display', $counter);
    $form_state->setRebuild();
  }

  /**
   * @param $form
   * @param $form_state
   *
   * @return mixed
   */
  public function customExcelFilterAddAjax($form, $form_state) {
    return $form['filter']['table'];
  }

  /**
   * Submit callback: Add Filter item.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function customExcelAddFilterItem(array &$form, FormStateInterface $form_state) {
    $this->filter_number_count++;
    $form_state->set('filter_number_count', $this->filter_number_count);
    $form_state->setRebuild();
  }

  /**
   * Callback: Ajax option to remove item.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function customExcelRemoveSortItem(array &$form, FormStateInterface $form_state) {

    $counter = $form_state->getTriggeringElement()['#sort_counter'];

    $form_state->set('remove_sort_item', $counter);
    $form_state->set('remove_sort_count', $this->sort_number_count);

    $this->remove_sort_item[$counter] = $counter;

    if ($this->sort_number_count > 1) {
      $this->sort_number_count--;
    }

    $form_state->setRebuild();
  }

  /**
   * Ajax callback: Remove Filter Item.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function customExcelRemoveFilterItem(array &$form, FormStateInterface $form_state) {

    $counter = $form_state->getTriggeringElement()['#filter_counter'];

    $form_state->set('remove_filter_item', $counter);
    $form_state->set('remove_filter_count', $this->filter_number_count);

    $this->remove_filter_item[$counter] = $counter;

    if ($this->filter_number_count > 1) {
      $this->filter_number_count--;
    }
    $form_state->setRebuild();
  }

  /**
   * Submit Callback: Remove display item.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function customExcelRemoveDisplayItem(array &$form, FormStateInterface $form_state) {

    $counter = $form_state->getTriggeringElement()['#display_counter'];

    $form_state->set('remove_display_item', $counter);
    $form_state->set('remove_display_count', $this->filter_number_count);

    if ($this->display_number_count > 1) {
      $this->display_number_count--;
    }

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    foreach ($form_state->getValue('sorting')['table'] as $sorting_value) {
      if (empty($sorting_value['column_name'])) {
        $form_state->setErrorByName('column_name', 'Sorting  column name is required');
      }
    }

    foreach ($form_state->getValue('filter')['table'] as $filter_value) {
      if (empty($filter_value['column_name'])) {
        $form_state->setErrorByName('column_name', 'Filter  column name is required');
      }
    }
  }

  /**
   * Saving form data to create entity
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $header_option = $form_state->getValue('excel_header');
    $excel_entity_id = $form_state->getValue('entity_id');
    $default_display_option = $form_state->getValue('default_display');
    $default_display = [];

    foreach ($default_display_option as $display_key => $display_option_value) {
      if ($display_option_value !== 0) {
        $default_display[$display_key] = $display_option_value;
      }
    }

    unset($header_option['']);

    // If there is excel entity id.
    $excel_entity = CustomExcelTable::load($excel_entity_id);
    $excel_entity->set('table_header', json_encode($header_option));
    $excel_entity->set('default_display_header', json_encode($default_display));

    // Creating Sorting option for entity.
    $sort_array = [];
    $sorting_values = $form_state->getValue('sorting')['table'];
    foreach ($sorting_values as $sort_value) {
      $sort_field_name = CustomExcelTableHelper::getFieldName($sort_value['column_name']);

      // Getting sorting paragraph id.
      $sort_par_id = $sort_value['entity_id'];

      if ($sort_par_id > 0) {
        $sort_paragraph = Paragraph::load($sort_par_id);
        $sort_paragraph->set('field_excel_sort_label', $header_option[$sort_value['column_name']]);
        $sort_paragraph->set('field_excel_sort_name', $sort_field_name);
        $sort_paragraph->set('field_excel_sort_order', $sort_value['sorting_type']);
        $sort_paragraph->save();
      }
      else {
        $sort_paragraph = Paragraph::create([
          'type' => 'excel_sort',
          'field_excel_sort_label' => $header_option[$sort_value['column_name']],
          'field_excel_sort_name' => $sort_field_name,
          'field_excel_sort_order' => $sort_value['sorting_type'],
        ]);

        $sort_paragraph->save();
      }

      $sort_array[] = [
        'target_id' => $sort_paragraph->id(),
        'target_revision_id' => $sort_paragraph->getRevisionId(),
      ];
    }

    $filter_array = [];
    $filter_values = $form_state->getValue('filter')['table'];

    foreach ($filter_values as $filter_value) {
      $filter_field_name = CustomExcelTableHelper::getFieldName($filter_value['column_name']);
      $filter_column_label = (!empty($filter_value['column_label']) ? $filter_value['column_label'] : $header_option[$filter_value['column_name']]);

      // Getting sorting paragraph id.
      $filter_par_id = $filter_value['entity_id'];

      if ($filter_par_id > 0) {
        $filter_paragraph = Paragraph::load($filter_par_id);
        $filter_paragraph->set('field_excel_filter_field_name', $filter_field_name);
        $filter_paragraph->set('field_excel_filter_field_type', $filter_value['field_type']);
        $filter_paragraph->set('field_excel_filter_label', $filter_column_label);
        $filter_paragraph->set('field_excel_filter_operator', $filter_value['operator']);
        $filter_paragraph->save();
      }
      else {
        $filter_paragraph = Paragraph::create([
          'type' => 'excel_filter',
          'field_excel_filter_field_name' => $filter_field_name,
          'field_excel_filter_field_type' => $filter_value['field_type'],
          'field_excel_filter_label' => $filter_column_label,
          'field_excel_filter_operator' => $filter_value['operator'],
        ]);
        $filter_paragraph->save();
      }

      $filter_array[] = [
        'target_id' => $filter_paragraph->id(),
        'target_revision_id' => $filter_paragraph->getRevisionId(),
      ];
    }

    // Show display array.
    $display_array = [];
    $display_values = $form_state->getValue('display_show')['display'];
    foreach ($display_values as $display_value) {
      $display_field_name = CustomExcelTableHelper::getFieldName($display_value['display_label']);

      // Getting sorting paragraph id.
      $display_par_id = $display_value['entity_id'];

      if ($display_par_id > 0) {
        $display_paragraph = Paragraph::load($display_par_id);
        $display_paragraph->set('field_excel_show_field_label', $display_value['display_label']);
        $display_paragraph->set('field_excel_show_field_name', $display_field_name);
        $display_paragraph->save();
      }
      else {
        $display_paragraph = Paragraph::create([
          'type' => 'excel_show',
          'field_excel_show_field_label' => $display_value['display_label'],
          'field_excel_show_field_name' => $display_field_name,
        ]);
      }

      // Creating option for display show.
      $display_options = $display_value['option']['option_wrap'];
      $option_array = [];

      foreach ($display_options as $option_value) {
        $option_field_name = CustomExcelTableHelper::getFieldName($option_value['option_label']);
        $option_val_arr = array_values($option_value['option_column']);

        $option_display_entity_id = $option_value['entity_id'];

        if (!empty($option_value['option_label']) || !empty($option_val_arr)) {

          if ($option_display_entity_id > 0) {
            $column_paragraph = Paragraph::load($option_display_entity_id);
            $column_paragraph->set('field_excel_column_label', $option_value['option_label']);
            $column_paragraph->set('field_excel_column_name', $option_field_name);
            $column_paragraph->set('field_excel_display_option', $option_val_arr);
            $column_paragraph->save();
          }
          else {
            $column_paragraph = Paragraph::create([
              'type' => 'excel_column',
              'field_excel_column_label' => $option_value['option_label'],
              'field_excel_column_name' => $option_field_name,
              'field_excel_display_option' => $option_val_arr,
            ]);
            $column_paragraph->save();
          }
        }

        if ($column_paragraph) {

          $option_array[] = [
            'target_id' => $column_paragraph->id(),
            'target_revision_id' => $column_paragraph->getRevisionId(),
          ];
        }
      }

      if ($display_paragraph) {
        $display_paragraph->set('field_excel_display_column', $option_array);
        $display_paragraph->save();

        $display_array[] = [
          'target_id' => $display_paragraph->id(),
          'target_revision_id' => $display_paragraph->getRevisionId(),
        ];
      }
    }

    $excel_entity->set('excel_filter', $filter_array);
    $excel_entity->set('excel_show', $display_array);
    $excel_entity->set('excel_sort', $sort_array);

    \Drupal::messenger()
      ->addMessage('Excel file is saved and it will show data after running cron jobs. Please contact administrator for update.');

    // Saving entity.
    $excel_entity->set('cron_flag', 0);
    $excel_entity->save();

    $form_state->setRedirect('entity.custom_excel_table.collection');

  }

  /**
   * Submit callback: Back button.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function customExcelBackSubmit(array &$form, FormStateInterface $form_state) {

    $entity_id = \Drupal::request()->query->get('entity_id');

    $excel_entity_id = $form_state->getValue('entity_id');
    $excel_entity = CustomExcelTable::load($entity_id);
    $form_state->setRedirect('entity.custom_excel_table.add_sheets', [], ['query' => ['entity_id' => $excel_entity->id()]]);
  }

}
