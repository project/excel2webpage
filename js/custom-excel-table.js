/**
 * @File
 * Custom Drop js File
 */

(function ($, Drupal, drupalSettings) {

  $(document).ready(function() {

    ///$('.custom-excel-table').dataTable().fnDestroy()
    var entity_id = drupalSettings.custom_excel_table.entity_id;
    var db_table = drupalSettings.custom_excel_table.db_table;
    var tablecolumn = drupalSettings.custom_excel_table.table_column;
    var tableheaders = drupalSettings.custom_excel_table.table_headers;
    var tableallcolumn = drupalSettings.custom_excel_table.table_column;
    var show_column = drupalSettings.custom_excel_table.show_column;
    var default_headers = drupalSettings.custom_excel_table.default_display_header;

    var filter_column = drupalSettings.custom_excel_table.filter_column;
    var order_column = drupalSettings.custom_excel_table.sort_column;

    console.log(order_column);

    var oTable = $('.custom-excel-table').DataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "/ajax/custom_excel_table/" + entity_id,
        "type": "POST",
        'data': {
          'entity_id': entity_id,
          'db_table': db_table,
          'table_column': tablecolumn,
          'table_headers': tableheaders,
          'column_filter': filter_column,
          'sort_column': order_column
        },
        dataSrc: 'data',
      },
      "order": order_column,
      "bDestroy": true,
      "scrollX": true,
      "scrollcollapse": true,
      "responsive": true
    });

    oTable.columns.adjust().draw();

    //hide column
    hideTableColumn(tableallcolumn);

    //showing specific column
    showTableColumn(default_headers);

    //Searching on key
    $('.form-text').on('keyup', function () {
      var column_name = $(this).attr('column-name');
      var column_number = search(column_name, tablecolumn);
      oTable.columns(column_number).search($(this).val()).draw();
    });

    $('.excel-filter .form-select').on('change', function () {
      var column_name = $(this).attr('column-name');
      var column_number = search(column_name, tablecolumn);
      oTable.columns(column_number).search($(this).val()).draw();
    });

    //Show/Hide column
    $('.excel-show .form-select').on('change', function () {

      //hide all column
      hideTableColumn(tableallcolumn);

      var selectedVal = $(this).val();
      if (selectedVal == '--') {
        var show_column_list = default_headers;
      } else {
        var column_name = $(this).attr('column-name');
        var show_column_values = show_column[column_name];
        var show_column_list = show_column_values[selectedVal];
      }
      showTableColumn(show_column_list);

    });

    //Search index of column
    function search(nameKey, myArray) {
      for (var i = 0; i < myArray.length; i++) {
        if (myArray[i] === nameKey) {
          return i;
        }
      }
    }

    //Hide column to show these later.
    function hideTableColumn(column_list) {
      for (colno in column_list) {
        var column_number = search(column_list[colno], tablecolumn);
        var column = oTable.column(column_number);
        // Toggle the visibility
        column.visible(false);
      }
    }

    function showTableColumn(column_list) {
      for (colno in column_list) {
        var column_number = search(column_list[colno], tablecolumn);
        var column = oTable.column(column_number);
        // Toggle the visibility
        column.visible(true);
      }
    }

    /**
    $('.custom-excel-table thead th').click(function(){
      $('.custom-excel-table thead th').each(function() {
        var th_index = $(this).index();
        var th_width = $(this).width();
        $('.custom-excel-table tbody td').eq(th_index).width(th_width);
      });

      oTable.columns.adjust().draw();
    })**/

  });
}(jQuery, Drupal, drupalSettings));
