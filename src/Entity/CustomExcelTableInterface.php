<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Drupal\excel2webpage\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Custom Excel Table.
 *
 * @ingroup custom_excel_table
 */
interface CustomExcelTableInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the store name.
   *
   * @return string
   *   The store name.
   */
  public function getTitle();

  /**
   * Sets the store name.
   *
   * @param string $name
   *   The store name.
   *
   * @return $this
   */
  public function setTitle($name);

}
