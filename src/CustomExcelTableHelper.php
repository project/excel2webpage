<?php

namespace Drupal\excel2webpage;

use Drupal\Core\Database\Database;
use Drupal\paragraphs\Entity\Paragraph;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 *
 */
class CustomExcelTableHelper {

  /**
   * Getting Filter for Excel.
   *
   * @param $entity
   */
  public static function getExcelFilter($entity) {

    $data_result = [];

    // If filter data is not empty.
    if (!$entity->get('excel_filter')->isEmpty()) {

      $filter_values = $entity->get('excel_filter')->getValue();
      foreach ($filter_values as $filter_data) {
        $filter_id = $filter_data['target_id'];
        $filter_entity = Paragraph::load($filter_id);
        // Getting fields for Filter values.
        $data_result[] = self::getParagraphData($filter_entity);
      }
    }
    return $data_result;
  }

  /**
   * Getting Fields array for paragrpah field.
   *
   * @param $filter_entity
   *
   * @return array
   */
  public static function getParagraphData($filter_entity) {
    $filter_data = [];
    $filter_fields = self::getEntityFields($filter_entity, $filter_entity->getEntityTypeId());

    foreach ($filter_fields as $field_name => $field_array) {

      if ($field_array['field_type'] == 'entity_reference_revisions') {
        $filter_data[$field_name] = self::getParagraphChild($filter_entity, $field_array);
      }
      else {
        if ($field_array['cardinality'] == 1) {
          $filter_data[$field_name] = (!$filter_entity->get($field_name)
            ->isEmpty() ? $filter_entity->get($field_name)
            ->getValue()[0]['value'] : '');
        }
        else {
          $filter_data[$field_name] = self::getMultiValue($filter_entity, $field_array);
        }
      }

    }
    return $filter_data;
  }

  /**
   * Calback: Getting Fields for Entity and Bundle.
   *
   * @param null $entity_node
   * @param null $entity_type
   */
  public static function getEntityFields($entity_node = NULL, $entity_type = NULL) {

    $fields_array = [];

    // Checking if EntityNode and Entity type is not null.
    if (!self::isEmpty($entity_node) and !self::isEmpty($entity_type)) {

      // Getting Entity Fields.
      $entity_fields = self::getEntityFieldDefinitions($entity_type, $entity_node->bundle());

      // Iterating through entity Fields.
      foreach ($entity_fields as $fields) {
        if ($fields->getFieldStorageDefinition()
          ->isBaseField() == FALSE) {

          $fields_child_array = self::getFieldsChildArray($fields);
          $fields_array[$fields->getName()] = $fields_child_array;
        }
      }
    }

    return $fields_array;
  }

  /**
   * Calback: Checking if Field is Empty or Not.
   *
   * @param $data
   *
   * @return bool
   */
  public static function isEmpty($data, $key = NULL) {

    if (!isset($data) || empty($data) || is_null($data)) {
      return TRUE;
    }
    if (isset($key)) {
      return !isset($data[$key]) || empty($data[$key]) || is_null($data[$key]);
    }
    return FALSE;
  }

  /**
   * Callback: Get Entity Field Definations.
   *
   * @param $entity_type
   * @param $entity_bundle
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   */
  public static function getEntityFieldDefinitions($entity_type, $entity_bundle) {
    $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $entity_bundle);
    return $definitions;
  }

  /**
   * Callback: Get child Array of fields.
   *
   * @param $fields
   *
   * @return array
   */
  public static function getFieldsChildArray($fields) {
    $fields_child_array = [];

    $fields_child_array['target_type'] = (!self::isEmpty($fields->getFieldStorageDefinition()
      ->getSetting('target_type')) ? $fields->getFieldStorageDefinition()
      ->getSetting('target_type') : '');

    $fields_child_array['name'] = $fields->getName();
    $fields_child_array['settings'] = $fields->getSettings();
    $fields_child_array['field_type'] = $fields->getType();

    $fields_child_array['cardinality'] = $fields->getFieldStorageDefinition()
      ->getCardinality();

    return $fields_child_array;
  }

  /**
   * Get paragrpaph child.
   *
   * @param $entity
   * @param $field_array
   */
  public static function getParagraphChild($entity, $field_array) {

    $result_data = [];
    $field_name = $field_array['name'];

    if (!$entity->get($field_name)->isEmpty()) {

      $filter_values = $entity->get($field_name)->getValue();
      foreach ($filter_values as $filter_data) {
        $filter_id = $filter_data['target_id'];
        $filter_entity = Paragraph::load($filter_id);
        // Getting fields for Filter values.
        $result_data[] = self::getParagraphData($filter_entity);

      }
    }

    return $result_data;
  }

  /**
   * Getting multi value field.
   *
   * @param $entity
   * @param $field_array
   */
  public static function getMultiValue($entity, $field_array) {
    $result_data = [];
    $field_name = $field_array['name'];
    foreach ($entity->get($field_name)->getValue() as $value) {
      $result_data[] = $value;
    }

    return $result_data;
  }

  /**
   * Getting Filter for Excel.
   *
   * @param $entity
   */
  public static function getExcelSort($entity) {

    $data_result = [];

    // If filter data is not empty.
    if (!$entity->get('excel_filter')->isEmpty()) {

      $filter_values = $entity->get('excel_sort')->getValue();
      foreach ($filter_values as $filter_data) {
        $filter_id = $filter_data['target_id'];
        $filter_entity = Paragraph::load($filter_id);
        // Getting fields for Filter values.
        $data_result[] = self::getParagraphData($filter_entity);

      }
    }
    return $data_result;
  }

  /**
   * Getting Filter for Excel.
   *
   * @param $entity
   */
  public static function getExcelShow($entity) {

    $data_result = [];

    // If filter data is not empty.
    if (!$entity->get('excel_filter')->isEmpty()) {

      $filter_values = $entity->get('excel_show')->getValue();
      foreach ($filter_values as $filter_data) {
        $filter_id = $filter_data['target_id'];
        $filter_entity = Paragraph::load($filter_id);
        // Getting fields for Filter values.
        $data_result[] = self::getParagraphData($filter_entity);

      }
    }
    return $data_result;
  }

  /**
   * Getting select option.
   *
   * @param $filter_data
   */
  public static function getSelectOption($show_data) {

    foreach ($show_data as $data) {
      $option_data[$data['field_excel_column_name']] = $data['field_excel_column_label'];
    }

    return $option_data;
  }

  /**
   * Getting select option.
   *
   * @param $filter_data
   */
  public static function getSelectOptionColumn($show_data) {

    $option_data = [];

    foreach ($show_data as $data) {

      $column_names = [];
      if (isset($data['field_excel_display_option'])) {
        foreach ($data['field_excel_display_option'] as $col_name) {
          $column_names[] = $col_name['value'];
        }
      }

      $option_data[$data['field_excel_column_name']] = $column_names;
    }

    return $option_data;
  }

  /**
   * Get Data for excel file.
   *
   * @param $excel_file
   *
   * @throws \PhpOffice\PhpSpreadsheet\Exception
   * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
   */
  public static function getExcelFileData($excel_file, $sheet = 0) {

    $excel_file_name = $excel_file->getFilename();
    $excelObj = self::getExcelFileObj($excel_file);
    $worksheet = $excelObj->getSheet($sheet);
    $lastRow = $worksheet->getHighestRow();
    $lastColumn = $worksheet->getHighestColumn();

    $excel_data = [];

    for ($row = 1; $row <= $lastRow; $row++) {
      $columndata = $worksheet->rangeToArray('A' . $row . ':' . $lastColumn . $row, NULL, TRUE, FALSE);
      $excel_data[] = $columndata;
    }

    $excel_update_data = self::mergeChild($excel_data);
    $data = self::convertArray($excel_update_data);

    // Removing trailing spacedata =.
    array_walk_recursive($data, function (&$v) {
      $v = trim($v);
    });

    $result_data = self::removeEmptyValueFromArray($data);

    return $result_data;
  }

  /**
   * Callback: Generate Excel File Object.
   *
   * @param $excel_file
   *
   * @return \PhpOffice\PhpSpreadsheet\Spreadsheet
   *
   * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
   */
  public static function getExcelFileObj($excel_file) {
    $excel_file_url = \Drupal::service('file_system')
      ->realpath($excel_file->getFileUri());

    // Read Excel file.
    $excel_reader = IOFactory::createReaderForFile($excel_file_url);
    $excelObj = $excel_reader->load($excel_file_url);
    return $excelObj;
  }

  /**
   * Convert array to read correct data.
   *
   * @param $data
   */
  public static function convertArray($data) {

    $json_heading = array_shift($data);
    // Remove special character.
    $headings = self::removeSpecialCharFromHeader($json_heading);

    array_walk(
      $data,
      function (&$row) use ($headings) {
        if (!empty($row)) {
          $row = array_combine($headings, $row);
        }
      }
    );

    return $data;
  }

  /**
   * Callback: Remove special character.
   *
   * @param $headings
   *
   * @return array
   */
  public static function removeSpecialCharFromHeader($headings) {
    $head_arr = [];
    foreach ($headings as $head_title) {
      $head_arr[] = trim(self::getFieldName(self::truncate($head_title, 32)));
    }

    return $head_arr;
  }

  /**
   * Getting field name.
   *
   * @param $field_name
   *
   * @return string
   */
  public static function getFieldName($field_name) {
    $field_name = strtolower(str_replace(' ', '_', str_replace([
      '\'',
      '"',
      ',',
      ';',
      '<',
      '>',
      '(',
      ')',
      '/',
      '.',
      '&amp;',
      '-',
      '&',
    ], '', $field_name), $field_name));

    return $field_name;

  }

  /**
   * Remove Special character.
   *
   * @param $string
   *
   * @return string|string[]
   */
  public static function removeSpecialCharacter($string) {

    $field_name = str_replace([
      '\'',
      '"',
      ',',
      ';',
      '<',
      '>',
      '(',
      ')',
      '/',
      '.',
      '&amp;',
      '-',
      '&',
    ], '', $string);

    return $field_name;

  }

  /**
   * Callback: Get list of excel sheets.
   *
   * @param $excel_file
   *
   * @return string[]
   *
   * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
   */
  public static function getExcelSheets($excel_file) {
    $excelObj = self::getExcelFileObj($excel_file);
    return $excelObj->getSheetNames();
  }

  /**
   * Callback: Getting Excel Header.
   *
   * @param $excel_file
   * @param int $sheet
   *
   * @return array
   *
   * @throws \PhpOffice\PhpSpreadsheet\Exception
   * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
   */
  public static function getExcelHeader($excel_file, $sheet = 0) {
    $excelObj = self::getExcelFileObj($excel_file);
    $worksheet = $excelObj->getSheet($sheet);
    $row = 1;
    $lastColumn = $worksheet->getHighestColumn();
    $header_data = [];
    $columndata = [];

    $worksheet->getColumnIterator();

    $columndata = $worksheet->rangeToArray('A' . $row . ':' . $lastColumn . $row, NULL, TRUE, FALSE);

    foreach ($columndata[0] as $column_value) {
      if (!empty($column_value)) {
        // $column_field_name = self::getFieldName($column_value);
        $column_field_name = self::getFieldName(self::truncate($column_value, 32));
        $header_data[$column_field_name] = $column_value;
      }
    }

    return $header_data;
  }

  /**
   * Get Option for select field.
   *
   * @param $tablename
   * @param $columnname
   */
  public static function getSelectResult($tablename, $columnname) {

    $excel_config = \Drupal::config('excel2webpage.settings');
    $db_connect = $excel_config->get('excel_db_connect');

    // Set active connection.
    Database::setActiveConnection($db_connect);

    $query = Database::getConnection()
      ->select($tablename, 'et')
      ->fields('et', [$columnname]);
    $result = $query->execute()->fetchAll();

    foreach ($result as $data) {
      $data_arr = (array) $data;
      $option[$data_arr[$columnname]] = $data_arr[$columnname];
    }

    $option[''] = '- Select -';
    asort($option);

    // Set active connection.
    Database::setActiveConnection('default');

    return $option;
  }

  /**
   * Callback: Check if excel configuration exist.
   *
   * @param $excel_config
   *
   * @return bool
   */
  public static function isExistExcelConfiguration($excel_config) {

    $flag = FALSE;

    if (!empty($excel_config->get('excel_db_connect'))) {
      $flag = TRUE;
    }
    return $flag;
  }

  /**
   * Callback: Remove empty sub array.
   *
   * @param $data_array
   *
   * @return array
   */
  public static function removeEmptyValueFromArray($data_array) {

    $result_data = $data_array;

    foreach ($data_array as $key => $item) {
      $item_flag = TRUE;
      foreach ($item as $item_key => $item_value) {
        if (!empty($item_value)) {
          $item_flag = FALSE;
        }
      }
      if ($item_flag) {
        unset($result_data[$key]);
      }
    }

    return $result_data;
  }

  /**
   * Truncate character.
   *
   * @param $string
   * @param int $length
   * @param string $append
   *
   * @return string
   */
  public static function truncate($string, $length = 100) {
    $string = trim($string);

    if (strlen($string) > $length) {
      $string = wordwrap($string, $length);
      $string = explode("\n", $string, 2);
      $string = $string[0];
    }

    return $string;
  }

  /**
   * @param $data
   *
   * @return array
   */
  public static function mergeChild($data) {

    $result = [];
    foreach ($data as $key => $item) {
      $result[$key] = $item[0];
    }

    return $result;
  }

}
