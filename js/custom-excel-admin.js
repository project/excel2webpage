/**
 * @File
 * Custom Drop js File
 */

(function ($, Drupal, drupalSettings) {

  Drupal.behaviors.CustomExcelAdmin = {
    attach: function attach(context) {

      $('.copy-default-display').once().click(function (e) {

        var default_selected = [];

        $('#edit-default-display .form-checkbox:checked').each(function() {
          default_selected.push($(this).val());
        });

        //Show confirmation box.
          if(confirm("Select the column from Default display. If you click on Ok, it will be override option in show/hide column")){
            $(this).parents('.excel-display-wrap-option').children('.display-option-column-wrap').find('select').find('option').each(function(){
              var option_value = $(this).val();
              if(default_selected.indexOf(option_value) >= 0){
                $(this).prop('selected', "selected");
              }
              else {
                $(this).prop('selected', false);
              }
            });

            $(this).parents('.excel-display-wrap-option').children('.display-option-column-wrap').find('select').trigger("chosen:updated");
          }

         e.preventDefault();

        });
    }
  }
}(jQuery, Drupal, drupalSettings));
