<?php

namespace Drupal\excel2webpage;

use Drupal\Core\Database\Database;

/**
 *
 */
class CustomExcelTableBatchProcess {

  /**
   * Insert Data in DB table.
   */
  public static function insertData($tablename, $insert_data, &$context) {

    if (is_array($insert_data)) {
      Database::getConnection()
        ->insert($tablename)
        ->fields($insert_data)
        ->execute();
    }

    $context['results'][] = "Excel";
    $context['message'] = t('Inserted Excel Row');

  }

}
