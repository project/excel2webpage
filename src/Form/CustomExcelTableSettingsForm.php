<?php

namespace Drupal\excel2webpage\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class InventorySettingsForm.
 *
 * @package Drupal\commerce_multivendor\Form
 * @ingroup manage_inventory
 */
class CustomExcelTableSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'custom_excel_table_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['excel2webpage.settings'];
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $excel_config = $this->configFactory->get('excel2webpage.settings');

    $form['message'] = [
      '#type' => 'item',
      '#markup' => 'Please update Database configuration for Excel Table.',
    ];

    $form['excel_db_connect'] = [
      '#type' => 'textfield',
      '#title' => 'DB connection',
      '#default_value' => $excel_config->get('excel_db_connect'),
      '#required' => TRUE,
      '#description' => 'DB connection key in settings.php',
      '#placeholder' => 'eg:default',
    ];

    $form['excel_cron_number'] = [
      '#type' => 'textfield',
      '#title' => 'Number of Excel execute in cron',
      '#default_value' => $excel_config->get('excel_cron_number'),
      '#required' => TRUE,
      '#placeholder' => 'eg:1',
    ];

    return $form;
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('excel2webpage.settings')
      ->set('excel_db_connect', $form_state->getValue('excel_db_connect'))
      ->set('excel_cron_number', $form_state->getValue('excel_cron_number'))
      ->save();

    \Drupal::messenger()->addMessage('Excel Entity configuration is updated.');

    if (\Drupal::request()->query->has('destination')) {
      $url = Url::fromUserInput(\Drupal::request()->query->get('destination'));
      $form_state->setRedirectUrl($url);
    }

  }

}
