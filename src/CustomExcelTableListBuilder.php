<?php

namespace Drupal\excel2webpage;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a list controller for content_entity_manage_inventory entity.
 *
 * @ingroup manage_inventory
 */
class CustomExcelTableListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {

    $excel_config = \Drupal::config('excel2webpage.settings');

    $path = \Drupal::service('path.current')->getPath();

    if (empty(CustomExcelTableHelper::isExistExcelConfiguration($excel_config))) {

      \Drupal::messenger()->addMessage("Please update configuration for Excel Table, then you can create Excel Table.");
      $url = Url::fromRoute("entity.custom_excel_table.settings", ['destination' => $path])->toString();
      return new RedirectResponse($url);
    }
    else {
      $destination = ['destination' => $path];
      $build['description'] = [
        '#markup' => t('Import data from Excel file . <a href=":run_cron">Run cron</a>.', [
          ':run_cron' => Url::fromRoute('system.run_cron', [], ['query' => $destination])->toString(),
        ]),
      ];

      $build['table'] = parent::render();
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the inventory list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['title'] = $this->t('Title');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\manage_inventory\Entity\Inventory $entity */
    $row['id'] = $entity->id();
    $row['title'] = $entity->toLink();
    return $row + parent::buildRow($entity);
  }

}
