<?php

namespace Drupal\excel2webpage\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\excel2webpage\Entity\CustomExcelTable;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\excel2webpage\SSPDataTables;

/**
 * ModalFormExampleController class.
 */
class CustomExcelTableDetails extends ControllerBase {

  /**
   * Getting details of Excel Table.
   *
   * @return string[]
   */
  public function getDetails(Request $request) {

    // DB table to use.
    $json_data = $_POST;

    $table = $json_data['db_table'];
    $table_column = $json_data['table_column'];
    $table_headers = $json_data['table_headers'];
    $column_filter = $json_data['column_filter'];
    $sort_column = $json_data['sort_column'];

    // Table's primary key.
    $primaryKey = 'et_id';
    $column_data = [];

    foreach ($table_column as $counter => $column_name) {
      $columns[] = ['db' => $column_name, 'dt' => $counter];
    }

    $excel_config = \Drupal::config('excel2webpage.settings');
    $db_connect = $excel_config->get('excel_db_connect');

    $db_connect = Database::getConnectionInfo($db_connect);

    // SQL server connection information.
    $sql_details = [
      'user' => $db_connect['default']['username'],
      'pass' => $db_connect['default']['password'],
      'db'   => $db_connect['default']['database'],
      'host' => $db_connect['default']['host'],
    ];

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */

    $data = SSPDataTables::simple($_POST, $sql_details, $table, $primaryKey, $columns, $column_filter);
    return new JsonResponse($data);
  }

  /**
   *
   */
  public function getTitle() {

    $path = \Drupal::service('path.current')->getPath();
    $title = '';

    if (preg_match('/custom_excel_table\/(\d+)/', $path, $matches)) {

      $entity_id = $matches[1];
      $entity = CustomExcelTable::load($entity_id);

      $title = $entity->getTitle();
    }

    return $title;
  }

}
