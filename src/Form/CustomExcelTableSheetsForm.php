<?php

namespace Drupal\excel2webpage\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\excel2webpage\CustomExcelTableHelper;
use Drupal\excel2webpage\Entity\CustomExcelTable;
use Drupal\file\Entity\File;

/**
 * Form controller for the manage_inventory entity edit forms.
 *
 * @ingroup commerce_multivendor
 */
class CustomExcelTableSheetsForm extends FormBase {

  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'custom_excel_table_sheets_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    //If there is excel file uploaded
    $excel_sheets = [];
    $sheet_access = FALSE;
    $excel_file_id = 0;

    $entity_id = \Drupal::request()->query->get('entity_id');
    if ($entity_id) {
      $entity = CustomExcelTable::load($entity_id);
      $excel_sheet_value = '';

      $excel_sheet_value = (!$entity->get('excel_sheet')
        ->isEmpty() ? $entity->get('excel_sheet')->getValue()[0]['value'] : '');

      $excel_file_id = $entity->get('excel_file')->getValue()[0]['target_id'];
      $excel_file = File::load($excel_file_id);
      $excel_sheets = CustomExcelTableHelper::getExcelSheets($excel_file);

      $sheet_access = TRUE;

      $form['excel_sheet'] = [
        '#type' => 'select',
        '#title' => 'Excel Sheet',
        '#required' => TRUE,
        '#options' => $excel_sheets,
        '#default_value' => $excel_sheet_value,
      ];

      $form['entity_id'] = [
        '#type' => 'hidden',
        '#value' => $entity_id
      ];


      $form['actions']['next'] = [
        '#type' => 'submit',
        '#button_type' => 'primary',
        '#value' => $this->t('Next'),
      ];

      $form['actions']['back'] = [
        '#type' => 'submit',
        '#value' => $this->t('Back'),
        '#submit' => ['::customExcelBackSubmit'],
      ];
    }
    else {
      $form['message'] = [
        '#type' => 'item',
        '#markup' => 'There is no excel entity exists. Please create entity using ' . Link::fromTextAndUrl(t('Add Excel Table'), Url::fromRoute('entity.custom_excel_table.add_form'))->toString()
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    \Drupal::request()->query->remove('destination');
    $excel_entity_id = $form_state->getValue('entity_id');
    $excel_entity = CustomExcelTable::load($excel_entity_id);
    $excel_entity->set('excel_sheet', $form_state->getValue('excel_sheet'));
    $excel_entity->save();

    $form_state->setRedirect('entity.custom_excel_table.add_selection_form', [], ['query' => ['entity_id' => $excel_entity->id()]]);
  }

  /**
   * Submit callback: Back button
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function customExcelBackSubmit(array &$form, FormStateInterface $form_state) {

    $excel_entity_id = $form_state->getValue('entity_id');
    $excel_entity = CustomExcelTable::load($excel_entity_id);
    $excel_entity->save();

    $form_state->setRedirect('entity.custom_excel_table.add_form', [], ['query' => ['entity_id' => $excel_entity->id()]]);
  }
}
