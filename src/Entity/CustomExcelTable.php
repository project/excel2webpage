<?php

namespace Drupal\excel2webpage\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Store Locations entity.
 *
 * @ingroup custom_excel_table
 *
 * @ContentEntityType(
 *   id = "custom_excel_table",
 *   label = @Translation("Custom Excel Table"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\excel2webpage\CustomExcelTableListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\excel2webpage\Form\CustomExcelTableForm",
 *       "edit" = "Drupal\excel2webpage\Form\CustomExcelTableForm",
 *       "delete" =
 *   "Drupal\excel2webpage\Form\CustomExcelTableDeleteForm",
 *     },
 *     "access" =
 *   "Drupal\excel2webpage\CustomExcelTableAccessControlHandler",
 *   },
 *   base_table = "custom_excel_table",
 *   data_table = "custom_excel_table_field_data",
 *   admin_permission = "administer custom_excel_table entity",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/custom_excel_table/{commerce_store_locations}",
 *     "edit-form" = "/admin/structure/excel-table/{custom_excel_table}/edit",
 *     "delete-form" = "/admin/structure/excel-table/{custom_excel_table}/delete",
 *     "collection" = "/admin/structure/excel-table"
 *   },
 *   field_ui_base_route = "excel2webpage.settings",
 * )
 */
class CustomExcelTable extends ContentEntityBase implements CustomExcelTableInterface {

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the inventory entity.'))
      ->setReadOnly(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('The store owner.'))
      ->setDefaultValueCallback('Drupal\custom_excel_table\Entity\CustomExcelTable::getCurrentUserId')
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 50,
      ]);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The name of the Store Locations entity.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t(''))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['table_info'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Table Info'))
      ->setDescription(t(''))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['table_header'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Table Header'))
      ->setDescription(t(''))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['default_display_header'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Default Display Header'))
      ->setDescription(t(''))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['excel_file'] = BaseFieldDefinition::create('file')
      ->setLabel('Excel File')
      ->setRequired(TRUE)
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'excel-file',
        'file_extensions' => 'xls xlsx xltx',
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'file',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'file',
        'weight' => -1,
        'third_party_settings' => [],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['excel_sheet'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Excel Sheet'))
      ->setDescription(t('The name of the Store Locations entity.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string',
        'weight' => -6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['json_file'] = BaseFieldDefinition::create('file')
      ->setLabel('Json File')
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'json-file',
        'file_extensions' => 'json',
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'file',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['excel_filter'] = BaseFieldDefinition::create('entity_reference_revisions')
      ->setLabel(t('Select Filters'))
      ->setDescription(t('Filter for Excel Table'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'paragraph')
      ->setSetting('handler_settings', ['target_bundles' => ['excel_filter' => 'excel_filter']])
      ->setTranslatable(TRUE)
      ->setCardinality(-1)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_paragraphs',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['excel_show'] = BaseFieldDefinition::create('entity_reference_revisions')
      ->setLabel(t('Display Mode'))
      ->setDescription(t('Show Option for Excel Table'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'paragraph')
      ->setSetting('handler_settings', ['target_bundles' => ['excel_show' => 'excel_show']])
      ->setTranslatable(TRUE)
      ->setCardinality(-1)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_paragraphs',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['excel_sort'] = BaseFieldDefinition::create('entity_reference_revisions')
      ->setLabel(t('Excel Sort'))
      ->setDescription(t('Show Option for Excel Sort'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'paragraph')
      ->setSetting('handler_settings', ['target_bundles' => ['excel_sort' => 'excel_sort']])
      ->setTranslatable(TRUE)
      ->setCardinality(-1)
      ->setDisplayOptions('form', [
        'type' => 'paragraphs',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setDescription(t('The store URL alias.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setCustomStorage(TRUE);

    $fields['cron_flag'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Cron Flag'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDefaultValue(FALSE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @return array
   *   An array of default values.
   *
   * @see ::baseFieldDefinitions()
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($name) {
    $this->set('title', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->getEntityKey('owner');
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

}
