<?php

namespace Drupal\excel2webpage\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\excel2webpage\CustomExcelTableHelper;
use Drupal\excel2webpage\Entity\CustomExcelTable;

/**
 * Form controller for the manage_inventory entity edit forms.
 *
 * @ingroup commerce_multivendor
 */
class CustomExcelTableDetailsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_excel_table_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $current_path = explode('/', \Drupal::service('path.current')->getPath());

    // Getting details of entity.
    if ($current_path[1] == 'custom_excel_table' && is_numeric($current_path[2])) {

      $entity_id = $current_path[2];
      $excel_entity = CustomExcelTable::load($entity_id);
      $column_headers = [];

      $form['description'] = [
        '#type' => 'item',
        '#markup' => (!$excel_entity->get('description')
          ->isEmpty() ? $excel_entity->get('description')
          ->getValue()[0]['value'] : ''),
      ];

      $form['#cache'] = ['max-age' => 0];

      // Getting table info.
      $excel_table_info = json_decode($excel_entity->get('table_info')
        ->getValue()[0]['value']);
      $excel_default_header = (array) json_decode($excel_entity->get('default_display_header')
        ->getValue()[0]['value']);

      // Getitng column header.
      if (!empty($excel_table_info->fields)) {
        $column_headers = (array) $excel_table_info->fields;
      }

      if (!empty($excel_table_info)) {

        // Getting filter for excel.
        $excel_filter = CustomExcelTableHelper::getExcelFilter($excel_entity);
        $excel_sort = CustomExcelTableHelper::getExcelSort($excel_entity);
        $excel_show = CustomExcelTableHelper::getExcelShow($excel_entity);
        $excel_header_info = $excel_entity->get('table_header')
          ->getValue()[0]['value'];

        if (!empty($excel_show[0]['field_excel_display_column'])) {

          if (!empty($excel_filter)) {

            $form['filter'] = [
              '#type' => 'container',
              '#attributes' => ['class' => 'excel-filter'],
              '#cache' => ['max-age' => 0],
            ];

            $filter_column = [];

            foreach ($excel_filter as $filter_data) {
              $filter_column[$filter_data['field_excel_filter_field_name']] = $filter_data['field_excel_filter_operator'];

              // If there is select field.
              if ($filter_data['field_excel_filter_field_type'] == 'select') {
                $filter_options = CustomExcelTableHelper::getSelectResult($excel_table_info->tablename, $filter_data['field_excel_filter_field_name']);
                $form['filter'][$filter_data['field_excel_filter_field_name']] = [
                  '#type' => $filter_data['field_excel_filter_field_type'],
                  '#title' => $filter_data['field_excel_filter_label'],
                  '#attributes' => [
                    'column-name' => $filter_data['field_excel_filter_field_name'],
                    'operator' => $filter_data['field_excel_filter_operator'],
                  ],
                  '#options' => $filter_options,
                ];
              }
              else {
                $form['filter'][$filter_data['field_excel_filter_field_name']] = [
                  '#type' => $filter_data['field_excel_filter_field_type'],
                  '#title' => $filter_data['field_excel_filter_label'],
                  '#attributes' => [
                    'column-name' => $filter_data['field_excel_filter_field_name'],
                    'operator' => $filter_data['field_excel_filter_operator'],
                  ],
                ];
              }

            }

            $form['#attached']['drupalSettings']['custom_excel_table']['filter_column'] = $filter_column;
          }

          $form['show'] = [
            '#type' => 'container',
            '#attributes' => ['class' => 'excel-show'],
            '#cache' => ['max-age' => 0],
          ];

          $default_display = $excel_show[0]['field_excel_display_column'][0];

          foreach ($default_display['field_excel_display_option'] as $column_option) {
            $excel_default_header[$column_option['value']] = $column_option['value'];
          }

          foreach ($excel_show as $show_id => $show_data) {
            // Getting option for select option.
            $excel_show_option = CustomExcelTableHelper::getSelectOption($show_data['field_excel_display_column']);
            $excel_show_option_attr[$show_data['field_excel_show_field_name'] . $show_id] = CustomExcelTableHelper::getSelectOptionColumn($show_data['field_excel_display_column']);
            $form['show'][$show_data['field_excel_show_field_name'] . $show_id] = [
              '#type' => 'select',
              '#title' => $show_data['field_excel_show_field_label'],
              '#options' => $excel_show_option,
              '#attributes' => ['column-name' => $show_data['field_excel_show_field_name'] . $show_id],
            ];
          }

          $sort_array = [];
          $sort_array_header = array_keys($column_headers);

          foreach ($excel_sort as $sort_value) {
            $column_number = array_search($sort_value['field_excel_sort_name'], $sort_array_header);
            $sort_array[] = [
              $column_number,
              strtolower($sort_value['field_excel_sort_order']),
            ];
          }

          $table_data = [];

          $json_headers = array_values((array) json_decode($excel_header_info));

          $form['custom-excel-table'] = [
            '#type' => 'table',
            '#header' => $json_headers,
            '#empty' => 'There are no items yet.',
            '#rows' => $table_data,
            '#attributes' => [
              'class' => ['custom-excel-table'],
            ],
            '#cache' => ['max-age' => 0],
          ];

          foreach ($column_headers as $header_number => $header_value) {
            if (!in_array($header_value, $excel_default_header)) {
              $excel_show_option_attr['default']['default'][$header_number] = ['value' => $header_value];
            }
          }

          $column_data_headers = array_keys($column_headers);

          $form['#attached']['drupalSettings']['custom_excel_table']['entity_id'] = $entity_id;
          $form['#attached']['drupalSettings']['custom_excel_table']['table_column'] = $column_data_headers;
          $form['#attached']['drupalSettings']['custom_excel_table']['table_headers'] = (array) json_decode($excel_header_info);
          $form['#attached']['drupalSettings']['custom_excel_table']['sort_column'] = $sort_array;
          $form['#attached']['drupalSettings']['custom_excel_table']['default_display_header'] = $excel_default_header;
          $form['#attached']['drupalSettings']['custom_excel_table']['show_column'] = $excel_show_option_attr;
          $form['#attached']['drupalSettings']['custom_excel_table']['db_table'] = $excel_table_info->tablename;
          $form['#attached']['library'][] = 'excel2webpage/datatables';

        }
        else {
          $form['message'] = [
            '#type' => 'item',
            '#markup' => 'There is no display for this Excel.',
          ];
        }
      }
      else {
        $form['message'] = [
          '#type' => 'item',
          '#markup' => 'Excel is not generated for this entity',
        ];
      }

      return $form;
    }
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement submitForm() method.
  }

}
